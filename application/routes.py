"""Routes for logged-in application."""
from flask import Blueprint, render_template, request, render_template_string
from flask_login import current_user
from flask import current_app as app

from application import db
from application.forms import PurchaseForm
from .assets import compile_auth_assets
from flask_login import login_required


# Blueprint Configuration
main_bp = Blueprint('main_bp', __name__,
                    template_folder='templates',
                    static_folder='static')
compile_auth_assets(app)


@main_bp.route('/', methods=['GET'])
@login_required
def dashboard():
    """Serve logged in Dashboard."""
    return render_template('dashboard.html',
                           title='Flask-Login Tutorial.',
                           template='dashboard-template',
                           current_user=current_user,
                           body="You are now logged in!")


@main_bp.route('/purchase', methods=['GET', 'POST'])
@login_required
def purchase():
    purchase_form = PurchaseForm(request.form)
    if request.method == 'POST':
        if purchase_form.validate():
            current_user.money -= int(request.form.get('items_amount')) * 10
            db.session.add(current_user)
            db.session.commit()
            return render_template_string('<h1> Purchase success </h1>')
    return render_template('purchase.html', form=purchase_form)
