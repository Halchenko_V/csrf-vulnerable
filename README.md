Ta witryna jest wraźliwa do CSRF atak.
Polega to na tym że jak widać w
 applcation/templates/purchase.html nie wykorzystano CSRF tokena w formie zakupu towaru.
Pozwala to odsyłać tą formę z kontekstu jakiej kolwiek strony korzystając z sesji użytkownika.

Poprawić ten problem można wbudowaniem i zprawdzaniem jednorazowego CSRF tokena w każdej formie.